# ============================================================================ #
# Author: Tancredi-Paul Grozav <paul@grozav.info>
# ============================================================================ #
# It seems that the latest version of JRE stand alone is 1.8.0 (version 8),
# while the JRE inside JDK advanced to Java 14.0.1 (version 14)
#
# So, in order to run newer apps, written with a newer JDK, you'll need a newer
# JRE - thus you'll have to install the JDK even if you're not compiling.
#
# Thanks to:
# https://www.marcobehler.com/guides/a-guide-to-java-versions-and-features

# ==============================- BEGIN JRE -================================= #
echo "Installing JRE ..." &&
(
  exit 0 && # Avoid installing JRE 
  # Resolve JRE
  jre_base_url="https://javadl.oracle.com/webapps/download/AutoDL" &&

  # Get these IDs from https://java.com/en/download/
  # Version: 8
  # Update: 271
  # Release date: 2020-10-20
  xd_co_f="NDJhODkyM2UtODQ3YS00NmEwLTg3NDQtMGE3NzE3ODcyZmE4" &&
  BundleId="243727" &&
  version_id="61ae65e088624f5aaa0b1d2d801acb16" &&
  jre_base_url="${jre_base_url}?xd_co_f=${xd_co_f}" &&
  jre_base_url="${jre_base_url}&BundleId=${BundleId}" &&
  jre_base_url="${jre_base_url}_${version_id}" &&  
  unset xd_co_f &&
  unset BundleId &&
  unset version_id &&

  cd /opt/ &&
  wget --progress=dot:giga ${jre_base_url} -O ./jre.tar.gz &&
  tar xf ./jre.tar.gz &&
  rm -f ./jre.tar.gz &&
  mv ./jre* ./jre &&
  echo "export JAVA_HOME=\"/opt/jre\"" >> ${HOME}/.bashrc &&
  echo "export PATH=\"\${JAVA_HOME}/bin:\${PATH}\"" >> ${HOME}/.bashrc &&
  # Load java - required for installing additional stuff
  . ${HOME}/.bashrc &&
  exit 0
) &&
# ===============================- END JRE -================================== #





# ==============================- BEGIN JDK -================================= #
echo "Installing JDK ..." &&
(
  exit 0 && # Avoid installing JDK
  # Resolve JDK url
  jdk_url="https://www.oracle.com/java/technologies/javase-jdk15-downloads.html"
  jdk_url="$(
    wget -qO- ${jdk_url} |
    grep "Linux x64 Compressed Archive" -A 10 |
    grep "_linux-x64_bin.tar.gz" |
    cut -d \' -f 8
  )" &&

  cd /opt/ &&
  wget \
    --header "Cookie: oraclelicense=accept-securebackup-cookie" \
    --progress=dot:giga \
    -O ./jdk.tar.gz \
    ${jdk_url} &&
  tar xf ./jdk.tar.gz &&
  rm -f ./jdk.tar.gz &&
  mv ./jdk* ./jdk &&
  echo "export JAVA_HOME=\"/opt/jdk\"" >> ${HOME}/.bashrc &&
  echo "export PATH=\"\${JAVA_HOME}/bin:\${PATH}\"" >> ${HOME}/.bashrc &&
  # Load java - required for installing eclipse plugins
  . ${HOME}/.bashrc &&
  exit 0
) &&
# ===============================- END JDK -================================== #

exit 0
# ============================================================================ #